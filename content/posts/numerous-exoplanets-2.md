Title: Numerous Exoplanets - One More
Date: 2015-02-14 07:40
Category: Utils
Tags: ios, code, astronomy
Slug: numerous-exoplanets-2
Author: Grumpy

![Numerous]({filename}/images/posts/numerous-icon-100.png) 

Okay, one more exoplanet related *number* for the [Numerous](http://www.numerousapp.com) app.  After submitting the [last post]({filename}/posts/numerous-exoplanets.md) I realized that many folks checked the raw result for Kepler exoplanet candidates, so I've added that to the stable as well.

Kepler exoplanet candidates:

<iframe src="http://n.numerousapp.com/e/osws52re6v9q" width="250" height="250" frameBorder="0" seamless scrolling="no"></iframe>

#### Links to these *Numbers*

  * [Kepler Candidate Exoplanets](http://n.numerousapp.com/m/osws52re6v9q)
  
#### References:

1.  [NASA Exoplanet Archive](http://exoplanetarchive.ipac.caltech.edu/)
