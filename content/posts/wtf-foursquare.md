Title: Daily WTF from @Foursquare
Date: 2014-07-23 18:07
Category: Grumps
Tags: software, wtf
Slug: wtf-from-foursquare
Author: Grumpy

So I received an email from the fine folks at [Foursquare](http://www.foursquare.com) tonight thanking me for having been one of the first million users, and wanting to let me know about a few things they have coming up, namely two new apps: Swarm and Foursquare.  Okay, no problem so far.

But as I continue reading, and here is where the WTF comes from, they state that the app used to check into locations and to notify friends of those checkins, will now be called Swarm.  Foursquare (the app I currently use) will gain some entirely new functionality and if I want to continue to check in at locations I (and all my friends and family) will have to download this Swarm app.  Yep, they flip-flopped the names, wtf.

Why not simply add the new features to the current app and roll out the new feature in a new app?  Instead, they've decided to inconvenience all of their current users (some 50 million per the email) by having us download a brand new app and transfer our credentials.  They say all of our pics and checkins will be waiting, but again, why not just add features in Swarm not currently in Foursquare to the current app and bring out a new one with the new features?

And now that I think about it, I wonder what will happen with the things I have tied to their API.  Will those continue to work despite this stupidity, or will [IFTTT](http://www.ifttt.com) and all custom tie-ins have to be rewritten.  And if not, how confusing will it be once there is another app, presumably at another API endpoint, with the same name as this one?

Sometimes I just have to wonder what these companies are thinking... or not.

On a more positive note, I do like the new logo for the app which I will apparently no longer be using :)
