Title: Chamberlain Site - Needs Work
Date: 2015-01-04 08:01
Category: Grumps
Tags: wtf
Slug: chamberlain-site-needs-work
Author: Grumpy

It is time for a new garage door opener here at Grump Central.  So after shopping around, and having decided on a [Chamberlain](http://www.chamberlain.com/), this morning I decide to head to the source to see what exactly are the differences between the models we have seen.

Fortunately, Chamberlain has with each model displayed a checkbox so that one can compare the various models.  So on the first page I choose two models which I cannot differentiate based on description, and then head to the second page where lies the third model.  Unfortunately, the Chamberlain site does not save the previous selections when moving between pages (argh # 1).

So after a few wasted minutes trying to assure myself that I had actually selected the other models appropriately, I tinkered with the include/exclude functionality until all three models were on the same page.  I selected all three, and clicked the Compare button, et voila!

But wait, this comparison page has so little information that I still cannot determine what the differences are between the three models, other than possibly the color (argh #2).

So, unless you want merely to look at the pretty pictures, it seems the Chamberlain website is of little actual use.

![Chamberlain Comparator]({filename}/images/posts/chamberlain-site-needs-work-600.jpg)