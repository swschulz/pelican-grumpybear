Title: Numerous Exoplanets
Date: 2015-02-11 19:00
Category: Utils
Tags: ios, code, astronomy
Slug: numerous-exoplanets
Author: Grumpy

![Numerous]({filename}/images/posts/numerous-icon-100.png) 

Back with a few more *numbers* for the [Numerous](http://www.numerousapp.com) app.  This time, they are astronomy related.

I have followed with some interest the string of planets discovered over the past decade or so, and the other day I was wondering how many they had found thus far.  Well, after a bit of snooping, I found [several](http://www.exoplanets.org/) [sites](http://www.exoplanet.eu/) which have data related to exoplanets.  Of course, finding the data on the web is nice, but I figured if the data are available, then it must be tracked in Numerous.

I was dreading have to screen scrape one of those sites to get the current counts.  Fortunately, I found a site which had an API tied to their data.  The [NASA Exoplanet Archive](http://exoplanetarchive.ipac.caltech.edu/) located at Caltech offers a SQL-like interface into their data sets, so I have taken three of the numbers of most interest to me and turned them into Numerous data points.

First is a count of all confirmed exoplanets.  This includes those confirmed by any means.

<iframe src="http://n.numerousapp.com/e/dgftxjjph6rs" width="250" height="250" frameBorder="0" seamless scrolling="no"></iframe>

The second is a count of all of those planets confirmed by the [NASA Kepler Mission](http://kepler.nasa.gov/).

<iframe src="http://n.numerousapp.com/e/osws52re6v9q" width="250" height="250" frameBorder="0" seamless scrolling="no"></iframe>

Third, is a count of all of those Kepler planets which fall into what is considered the *habitable* [1] range.  This includes both confirmed and candidate planets. 

<iframe src="http://n.numerousapp.com/e/1wjloyhnu4rg4" width="250" height="250" frameBorder="0" seamless scrolling="no"></iframe>


#### Links to these *Numbers*

  * [All Confirmed Exoplanets](http://n.numerousapp.com/m/dgftxjjph6rs)
  * [Kepler Confirmed Exoplanets](http://n.numerousapp.com/m/osws52re6v9q)
  * [Kepler Habitable Zone Exoplanets](http://n.numerousapp.com/m/1wjloyhnu4rg4)
  
#### Footnotes

1.  (180 K < Equilibrium (T) < 310 K) or
(0.25 < Insolation (Earth flux) < 2.2)
