Title: Toying with Tabitop
Date: 2014-08-02 09:08
Category: Miscellanea
Tags: software, ios
Slug: toying-with-tabitop
Author: Grumpy

For many years, I have been a user of Linux on my laptops and desktops.  This year, I expanded that to include a Macbook Pro.  And??  Well, it means I rarely have need of a Windows machine any more, but still, there are those occasional apps which require it, e.g. the [GRLevel3](http://grlevelx.com/) weather application and the [ATCS Monitor](http://www.atcsmon.com/) for when I want to get my Railfan on.

So when I heard of [Tabitop](http://www.tabitop.com/), a new Desktop as a Service (DaaS) provider which was tailor made for iOS users, I decided to give it a try.

Their website lists [several plans](http://www.tabitop.com/pricing/) from which one can choose, depending on the amount of usage you expect.  Well since I rarely get out railfanning these days I decided to start with their Starter plan.  After signing up for the service, which took mere minutes, and agreeing to pay for the plan (it is charged through  your iTunes account), I was ready to go.

Since they provide either Windows 8 or Windows 7 (technically, they provide a Win8 or Win7 *experience* by building  your desktop either from Win Server 2012 or 2008 respectively) I chose the saner Windows 7 to start with.  After giving a name to the new VM (virtual machine) the build process began.  This took about 12 minutes and then I had a running machine.

Once complete, I was presented with a Windows desktop with several icons on it for such things as connecting to my OneDrive, Dropbox, or to install Microsoft Office.  I looked at those, and checked out the Start menu.  Since it was late, I told the machine to shutdown.  It was at this point that I was most surprised.

Each Tabitop plan comes with a set number of hours of usage, bandwidth, and I/O.  The Starter plan includes 20 hours of usage, 1 gig of bandwidth, and 1 million I/O units.  Knowing that I was not going to be a heavy user this seemed like plenty to me.  But when the machine was shut down, I noticed that it had already consumed 20 percent (200,000) of its allotted IO units.  Since I hadn't even opened the first application, that seemed excessive.  Given that rate of consumption, I'm not sure I would even get 1 out of the allotted 20 hours before the IO grant was consumed.

![Tabitop Screenshot]({filename}/images/posts/tabitop-600.jpg)

I will continue to play with the service until the IO expires, just to see how well the system works when used from an IOS device, but after that I'll cancel the account and continue to use a [TeamViewer](http://www.teamviewer.com/) based solution.

Find them on Twitter: @TabitopPC
