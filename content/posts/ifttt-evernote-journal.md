Title: IFTTT + Evernote = Journal? 
Date: 2014-06-07 14:06
Category: Utils
Tags: ifttt, evernote, journal
Slug: ifttt-evernote-journal
Author: Grumpy

I am an intermittent journaller, that is, I have a paper journal in which I write the days activities, but I do so only very infrequently.  So I've been thinking about adding on an electronic journal in the hope that it might lead me to add entries more often, since I am almost always at one computer or another, or I have my iPhone or iPad handy.

I've played with various solutions, e.g. [RedNotebook](http://rednotebook.sourceforge.net/) on Linux, [Day One](http://dayoneapp.com) and [Narrato](https://www.narrato.co) on iPhone.  Each has something I like:  RedNotebook offers complete control over the files, Day One has a nice interface, and Narrato does a good job of pulling in lifestream type data (and it can create public journals), but each also has things I don't like, so I thought I would use some tools I am already using, [IFTTT](http://www.ifttt.com) and [Evernote](http://www.evernote.com).

After thinking about it for a few days, I decided that I would have IFTTT create a new note in Evernote every morning, and add the day's weather forecast.  Then it could add lifestream entries to the note throughout the day for things like Instagram photos, Tweets, Foursquare checkins, etc.  For the journal part, I planned to use the iPhone app [Drafts](http://agiletortoise.com/drafts) to add journal entries to the note throughout the day.

So far, so good.  The problem arose when I actually tried to implement it.  I knew IFTTT could create new notes in Evernote, send a weather forecast at Sunrise, process the other items, etc., but the deal breaker arose when I started to create the first recipe.  I quickly learned that IFTTT has no way (that I can find) to add common things such as the current date or time to events.  I hadn't even considered that they would not have some basic macro functionality for things like that.

Not wanting to give up, I did some searching online, and even thought about writing a simple script to send an email to IFTTT every morning with a title which included the current date, and have IFTTT create the note via that hook, but then it would have no way to reference the note throughout the day to add items.

At this point, I decided to try another solution I've heard mentioned over the last year or so, [Zapier](http://zapier.com).  Zapier is basically an IFTTT for business people, that is, it is designed to interact with things like Salesforce and other business-type sites.  Along with that comes a hefty price, as well, but they do offer a very limited personal account for free.  So I signed up, connected it to my Evernote account, and set about creating my first recipe (Zap in their lingo).  I was pleasantly surprised to find that they do have a macro language, but saddened to find that it offered no way to format the day, so while I could create a new note with the current date, it was formatted as 2014-6-7, as opposed to the more universal 2014-06-07.  But hey, beggars and all that….  

However, while they do support many of the sites I wanted to extract data from, the free account is simply too limited, one is only able to process 100 items per month.  Since 30 would go to simply creating the note each morning,  a few tweets and/or Foursquare checkins per day would exhaust that number well before the end of the month.  And the lowest paid account runs $15 USD per month.  Significantly more than this project is worth.

So where does this leave me?  While I can hold out hope that IFTTT will add a basic macro language to its toolkit, I think the best I can hope for is to have it create the note with the weather but rather than adding the date to the title, to simply entitle it something generic, e.g. LOG, and then have all the other apps use that name to apped data throughout the day.  At that point, I will have to try to use the Evernote API to figure out how to rename that note sometime before the end of the day (or do it manually).  

If anyone has any other ideas, or has some similar working solution, I would love to hear about it.

—  GB
