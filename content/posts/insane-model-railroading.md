Title: Insane Model Railroading
Date: 2014-08-09 08:08
Category: Miscellanea
Tags: Trains
Slug: insane-model-railroading
Author: Grumpy

This is supposedly a world record for the number of cars in a coal train.  But, regardless if that is true or not, this is an insanely long model train.  I can only imagine the amount of time (and money) it took to put this together. 241 cars and 9 (!!) engines.

<iframe width="560" height="315" src="//www.youtube.com/embed/wevbi_zEmxc" frameborder="0" allowfullscreen></iframe>

