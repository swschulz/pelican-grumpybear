Title: Hello World
Date: 2014-06-01 06:00
Category: Miscellanea
Tags: miscellanea
Slug: hello-world
Author: Grumpy

Every blog needs one, don't it?  Continuing to play with [Pelican](http://getpelican.com) as a blogging platform, this time hosted on [Amazon S3](http://amazonaws.com).

Still tweaking things, but if this is posted, things are coming along.

GB
