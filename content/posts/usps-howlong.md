Title: @USPS - How Long?
Date: 2015-07-30 19:30
Category: Miscellanea
Tags: wtf
Slug: usps-howlong
Author: Grumpy

Just how is it possible that the [United States Postal Service](http://www.usps.com) can ship this package across several states over a weekend, but can't manage to get it delivered across town in four weekdays?

Sometimes this stuff just makes me shake my head.

![USPS Tracking]({filename}/images/posts/usps-howlong-600.jpg) 
