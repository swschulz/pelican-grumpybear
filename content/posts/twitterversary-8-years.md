Title: Twitterversary - 8 Years
Date: 2014-12-13 21:12
Category: Misc
Tags: miscellanea
Slug: twitterversary-8-years
Author: Grumpy

Received a notification from [Twitter](https://www.twitter.com) this morning notifying me that I've been tweeting for eight (8) years now.  Who'd have thought that it has been that long?

While I don't use the service as often as I used to, I still find it to be the best way of getting the word out to folks about what is going on, or of finding out what is happening in the world.

![Twitterversary Card]({filename}/images/posts/twitterversary-8years-600.jpg)
