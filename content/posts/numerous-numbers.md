Title: Numerous Numbers
Date: 2014-06-01 09:00
Category: Utils
Tags: ios, code
Slug: numerous-numbers
Author: Grumpy

![Numerous]({filename}/images/posts/numerous-icon-100.png) 

Being both a tech and numbers geek, I am compelled to check out all of the new toys which provide a way to monitor things (Nagios/Cacti anyone?)  So when I got wind of a new app called [Numerous](http://www.numerousapp.com/) I immediately had to download it and try it out.

Numerous is a tool to "[t]rack and share life’s most important numbers."  In short, it is an easy way to keep track of anything which has a changing value.  Among the *numbers* which come with it out of the box are such things as Atmospheric CO2, the current Bitcoin/USD exchange rate, various temperature displays, and a countdown until some event panel.  I added a few of these, but was really interested more in what I could add of my own.

Numerous has been added as a channel to [IFTTT](http://ifttt.com), so I can track my [Fitbit](http://www.fitbit.com) stats, or my vehicles mileage via the [Automatic](http://www.automatic.com) device (both of which also have IFTTT channels.  But better than that, they have a dead simple REST API, so if you can measure it, you can track it on numerous.

Thus far I am tracking the temperature in our datacenter via several *numbers* which display both the general room temp and the temps in a couple of the server racks, the number of active incidents on the [Raleigh-Wake ECC Incidents](http://incidents.rwecc.com) site, and others.  Since these likely have no one else who would be interested, I've kept these numbers private.

Among the public *numbers* I have implemented are two which display the height of the Neuse River at both Clayton and Smithfield, NC.  Since these may be of use to fishermen or kayakers in the area, they are publicly available.  Currently there is no central repository or search to find *numbers* beyond the few recommended or referenced in the app, so I am going to link to them here.

![Neuse River Height at Clayton, NC]({filename}/images/posts/numerous-neuse-clayton-200.jpg)
![Neuse River Height at Smithfield, NC]({filename}/images/posts/numerous-neuse-smithfield-200.jpg)

#### Links to these *Numbers*

  * [Neuse River Height at Clayton, NC](http://n.numerousapp.com/m/1rmnuywn7tk2v)
  * [Neuse River Height at Smithfield, NC](http://n.numerousapp.com/m/jw9wrjgtzwd)
  
#### Other Sites

One other site with quite a few *numbers* is run by [Limited Securities](http://limitedsecurities.com/numerous.aspx).  Check out their offerings as well.

