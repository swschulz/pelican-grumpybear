Title: VMware - Pay but not Download
Date: 2014-11-01 11:11
Category: grumps
Tags: software
Slug: vmware-pay-no-download
Author: Grumpy

Finally decided to go ahead and update the [VMware](http://www.vmware.com) Fusion on my rMBP to version 7 this morning.  Went through the entire process, only to find that the download functionality was under maintenance :(

Would be nice if they would take both sides of the transaction offline at the same time, or at least notify us that we can pay for but not download the software.

![VMware Under Maintenance]({filename}/images/posts/vmware-maint.jpg)