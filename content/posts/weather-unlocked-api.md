Title: Weather Unlocked - An Interesting API
Date: 2014-06-27 14:06
Category: Code
Tags: code, python, weather
Slug: weather-unlocked-api
Author: Grumpy

Being a weather hound, when I heard tell of a new weather service becoming available, I just had to explore.  A quick perusal through the [Weather Unlocked](http://www.weatherunlocked.com/) website reveals that they are primarily targeting their services at advertising-type users, e.g. to target ads at consumers based on weather conditions, but still I decided to look a little further.

Since they have a free tier for their service I signed up and began to explore the API, which is dead simple but which has an interesting query language.  For example, one would use `current precipitationtype eq anyrain` to find out whether it is currently raining in a location, or `current windspeed lt 5 mph` to find out whether there is enough breeze to get the kite off the ground.

Locations are specified either as Lat,Lon pairs, or using location codes, e.g. zip codes for US residents.  Putting it all together in a quickie Python script yields the following to determine whether it is going to be hot tomorrow:



    #!/usr/bin/env python
    
    import requests
    
    params = {
        'app_id': '<<your_app_id>>',
        'app_key': '<<your_app_key>>'
    }
    
    base_url = 'http://api.weatherunlocked.com/api/trigger/{location}/{query}'
    
    location = 'us.27520'
    query = 'forecast tomorrow temperature gt 70 f'
    
    r = requests.get(base_url.format(location=location, query=query), params=params)
    j = r.json()
    
    print 'Will it be hot tomorrow? {}'.format(j['ConditionMatched'])
    
    $ ./hot_tomorrow.py 
    Will it be hot tomorrow? Yes

Certainly an interesting API.  Not sure if/what I could use it for, but I give them credit for developing an innovative service.

@weatherunlocked
